using System;
using System.Runtime.InteropServices;

namespace Search {

	public class Entry : Gtk.Entry {

		public Entry (IntPtr raw) : base (raw) {}

		[DllImport("libsearchglue.so")]
		static extern IntPtr nld_search_entry_new ();

		public Entry () : base (IntPtr.Zero)
		{
			if (GetType () != typeof (Entry)) {
				CreateNativeObject (new string [0], new GLib.Value[0]);
				return;
			}
			Raw = nld_search_entry_new ();
		}

		[DllImport("libsearchglue.so")]
		static extern IntPtr nld_search_entry_get_type();

		public static new GLib.GType GType { 
			get {
				return new GLib.GType (nld_search_entry_get_type ());
			}
		}
	}
}
